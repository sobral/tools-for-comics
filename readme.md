Simple files to help organizing digital comic books.

There's a simple BAT file to convert several folders to CBZ using [7-zip](http://7-zip.org/):

- maximum compression;
- excluding `Thumbs.db` files.

And Photoshop scripts to help create double pages:

- even + odd, as standard;
- odd + even, because comic books have sometimes extra pages added or removed, breaking the standard.